FROM node:14.2.0

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn

COPY . .

EXPOSE 3977

CMD ["yarn", "start"]
